import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import{ SmtpPage } from '../pages/smtp/smtp';
import{ Pop3Page } from '../pages/pop3/pop3';
import{ PopvimapPage } from '../pages/popvimap/popvimap';
import{ ImapPage } from '../pages/imap/imap';
import{ AboutPage } from '../pages/about/about';
import{ ReferencesPage } from '../pages/references/references';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SmtpPage,
    Pop3Page,
    ImapPage,
    PopvimapPage,
    AboutPage,
    ReferencesPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SmtpPage,
    Pop3Page,
    ImapPage,
    PopvimapPage,
    AboutPage,
    ReferencesPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
