import { Component } from '@angular/core';
import { NavController, ViewController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ImapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-imap',
  templateUrl: 'imap.html',
})
export class ImapPage {

  constructor(public navCtrl: NavController, public viewCtrl: ViewController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ImapPage');
  }
  close(){
    this.viewCtrl.dismiss();
  }

}
