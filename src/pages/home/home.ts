import { Component } from '@angular/core';
import { NavController, PopoverController } from 'ionic-angular';
import{ SmtpPage } from '../smtp/smtp';
import{ Pop3Page } from '../pop3/pop3';
import{ ImapPage } from '../imap/imap';
import{ PopvimapPage } from '../popvimap/popvimap';
import{ AboutPage } from '../about/about';
import{ ReferencesPage } from '../references/references';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public popoverCtrl: PopoverController) {
  }

  presentSMTP() {
    // let popover = this.popoverCtrl.create(SmtpPage);
    // popover.present();
    this.navCtrl.push(SmtpPage);
  }

  presentPOP3() {
    // let popover = this.popoverCtrl.create(Pop3Page);
    // popover.present();
    this.navCtrl.push(Pop3Page);
  }

  presentIMAP() {
    // let popover = this.popoverCtrl.create(ImapPage);
    // popover.present();
    this.navCtrl.push(ImapPage);
  }
  presentPOPVIMAP() {
    // let popover = this.popoverCtrl.create(ImapPage);
    // popover.present();
    this.navCtrl.push(PopvimapPage);
  }

  aboutPage() {
    this.navCtrl.push(AboutPage);
  }
  referencesPage() {
    this.navCtrl.push(ReferencesPage);
  }

}
