import { Component } from '@angular/core';
import { NavController, ViewController, NavParams } from 'ionic-angular';

/**
 * Generated class for the Pop3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-pop3',
  templateUrl: 'pop3.html',
})
export class Pop3Page {

  constructor(public navCtrl: NavController, public viewCtrl: ViewController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Pop3Page');
  }
  close(){
    this.viewCtrl.dismiss();
  }

}
